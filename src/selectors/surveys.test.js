import { getSelectedSurvey, getSurveys } from './surveys'

describe('Surveys Selectors', () => {
  test('getSelectedSurvey is working properly', () => {
    expect(getSelectedSurvey({
      surveys: {
        active: 1,
        byId: {
          1: {
            name: 'Test'
          }
        }
      }
    })).toMatchObject({
      name: 'Test'
    })
  })

  test('getSurveys is working properly', () => {
    expect(getSurveys({
      surveys: {
        active: 1,
        byId: {
          1: {
            name: 'Hello'
          },
          2: {
            name: 'World'
          }
        }
      }
    })).toMatchObject([
      {
        name: 'Hello'
      },
      {
        name: 'World'
      }
    ])
  })
})
