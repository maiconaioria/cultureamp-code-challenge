import { isAppLoading } from './app'

describe('App Selectors', () => {
  test('isAppLoading is working properly', () => {
    expect(isAppLoading({
      app: {
        isLoading: false
      }
    })).toBe(false)
    expect(isAppLoading({
      app: {
        isLoading: true
      }
    })).toBe(true)
  })
})
