export const getSelectedSurvey = store => store.surveys.byId[store.surveys.active]

export const getSurveys = store => Object.values(store.surveys.byId)
