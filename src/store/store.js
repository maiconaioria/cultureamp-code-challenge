import { createStore, combineReducers, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
// reducers
import App from '../reducers/app'
import Surveys from '../reducers/surveys'

const logger = (store) => (next) => (action) => {
  const returnValue = next(action)
  if (process.env.NODE_ENV !== 'test') {
    // hide logging info when running tests
    console.log(action, store.getState())
  }
  return returnValue
}

const enhancer = composeWithDevTools(applyMiddleware(ReduxThunk, logger))

const reducers = combineReducers({
  app: App,
  surveys: Surveys
})

export const store = createStore(reducers, enhancer)
