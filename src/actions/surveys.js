// constants
import { FETCH_SURVEY, FETCH_SURVEYS, SELECT_SURVEY, URLS } from './constants'
// actions
import { toggleAppLoading } from './app'
// utils
import { fetchApi } from '../utils/api'
import { parseSurveyUrl } from '../utils/surveys'

export const selectSurvey = (surveyId = false) => async (dispatch) => {
  dispatch({
    type: SELECT_SURVEY,
    payload: {
      surveyId
    }
  })
}

export const fetchSurveys = () => async (dispatch) => {
  return fetchApi(URLS.SURVEY_LIST).then(response => response.survey_results).then(
    list => dispatch({
      type: FETCH_SURVEYS,
      payload: {
        list
      }
    })
  )
}

export const fetchSurvey = (id) => async (dispatch) => {
  return fetchApi(parseSurveyUrl(id)).then(response => response.survey_result_detail)
    .then(
      details => dispatch({
        type: FETCH_SURVEY,
        payload: {
          details
        }
      })
    )
}

export const setupSurvey = (surveyId) => async (dispatch) => {
  await dispatch(toggleAppLoading(true))
  await dispatch(selectSurvey(surveyId))
  await dispatch(fetchSurvey(surveyId))
  await dispatch(toggleAppLoading(false))
}

export const setupSurveys = () => async (dispatch) => {
  await dispatch(toggleAppLoading(true))
  await dispatch(selectSurvey())
  await dispatch(fetchSurveys())
  await dispatch(toggleAppLoading(false))
}
