export const TOGGLE_LOADING = 'TOGGLE_LOADING'
export const FETCH_SURVEY = 'FETCH_SURVEY'
export const FETCH_SURVEYS = 'FETCH_SURVEYS'
export const SELECT_SURVEY = 'SELECT_SURVEY'
export const CHANGE_USER = 'CHANGE_USER'

export const BASE_API_URL = 'https://px2yf2j445.execute-api.us-west-2.amazonaws.com/production'

export const URLS = {
  SURVEY_LIST: `${BASE_API_URL}/surveys`,
  SURVEY: `${BASE_API_URL}/surveys/{id}`
}
