// constants
import { TOGGLE_LOADING } from './constants'

export const toggleAppLoading = (flag) => {
  return {
    type: TOGGLE_LOADING,
    payload: flag
  }
}
