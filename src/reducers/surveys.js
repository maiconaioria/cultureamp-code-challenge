// constants
import { FETCH_SURVEY, FETCH_SURVEYS, SELECT_SURVEY } from '../actions/constants'

const surveys = {
  byId: {},
  active: false
}

const Surveys = (state = surveys, action) => {
  switch (action.type) {
    case FETCH_SURVEYS: {
      const { list } = action.payload
      return {
        ...state,
        byId: {
          ...state.byId,
          ...list.reduce((byId, survey) => {
            const id = survey.url.split('/').slice(-1)[0]
            return {
              ...byId,
              [id]: { ...survey, id }
            }
          }, {})
        }
      }
    }
    case FETCH_SURVEY: {
      const { details } = action.payload
      return {
        ...state,
        byId: {
          ...state.byId,
          [state.active]: {
            ...state.byId[state.active],
            ...details
          }
        }
      }
    }
    case SELECT_SURVEY: {
      const { surveyId } = action.payload
      return {
        ...state,
        active: surveyId
      }
    }
    default: {
      return state
    }
  }
}

export default Surveys
