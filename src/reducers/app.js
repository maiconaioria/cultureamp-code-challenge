// constants
import { TOGGLE_LOADING } from '../actions/constants'

const app = {
  isLoading: false
}

const App = (state = app, action) => {
  switch (action.type) {
    case TOGGLE_LOADING: {
      return {
        ...state,
        isLoading: action.payload
      }
    }
    default: {
      return state
    }
  }
}

export default App
