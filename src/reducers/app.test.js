// reducer
import App from './app'
// constants
import { TOGGLE_LOADING } from '../actions/constants'
// mock
import { mockState } from '../mock/app'

describe('App Reducer', () => {
  test('TOGGLE_LOADING action is working properly', () => {
    expect(
      App(mockState, {
        type: TOGGLE_LOADING,
        payload: true
      })
    ).toMatchObject({
      isLoading: true
    })

    expect(
      App(mockState, {
        type: 'RANDOM_ACTION',
        payload: true
      })
    ).toMatchObject({
      isLoading: false
    })
  })
})
