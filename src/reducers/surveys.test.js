// reducer
import Surveys from './surveys'
// constants
import { FETCH_SURVEY, FETCH_SURVEYS, SELECT_SURVEY } from '../actions/constants'
// mock
import { newSurvey, newSurveys, mockState } from '../mock/surveys'

describe('Surveys Reducer', () => {
  test('SELECT_SURVEY action is working properly', () => {
    expect(
      Surveys(mockState, {
        type: SELECT_SURVEY,
        payload: {
          surveyId: 1
        }
      })
    ).toMatchObject({
      ...mockState,
      active: 1
    })

    expect(
      Surveys(mockState, {
        type: 'RANDOM_ACTION',
        payload: {
          id: 1
        }
      })
    ).toMatchObject(mockState)
  })

  test('FETCH_SURVEY action is working properly', () => {
    expect(
      Surveys({
        ...mockState,
        active: newSurvey.id
      }, {
        type: FETCH_SURVEY,
        payload: {
          details: newSurvey
        }
      })
    ).toMatchObject({
      ...mockState,
      active: newSurvey.id,
      byId: {
        ...mockState.byId,
        [newSurvey.id]: {
          ...mockState.byId[newSurvey.id],
          ...newSurvey
        }
      }
    })
  })

  test('FETCH_SURVEYS action is working properly', () => {
    expect(
      Surveys(mockState, {
        type: FETCH_SURVEYS,
        payload: {
          list: newSurveys
        }
      })
    ).toMatchObject({
      ...mockState,
      byId: {
        ...mockState.byId,
        999: {
          id: '999'
        },
        666: {
          id: '666'
        }
      }
    })
  })
})
