import React from 'react'
import { useSelector } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
// components
import { PageLoader } from '../components'
// containers
import Survey from './Survey'
import Surveys from './Surveys'
// selectors
import { isAppLoading } from '../selectors/app'

// TODO: improve lists visual
// TODO: improve page loader visual
// TODO: use typescript

const App = () => {
  const isLoading = useSelector(isAppLoading)

  return (
    <Router>
      <Switch>
        <Route path="/survey" component={Survey} />
        <Route path="/" component={Surveys} />
      </Switch>
      <PageLoader isVisible={isLoading} />
    </Router>
  )
}

export default App
