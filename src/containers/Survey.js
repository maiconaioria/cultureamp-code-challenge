import React, { useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
// components
import {
  BackButton,
  Container,
  H1,
  Themes,
  P
} from '../components'
// selectors
import { getSelectedSurvey } from '../selectors/surveys'
// actions
import { setupSurvey } from '../actions/surveys'

const SurveyHOC = () => {
  const dispatch = useDispatch()

  const location = useLocation()
  const survey = useSelector(getSelectedSurvey)

  useEffect(() => {
    const surveyId = location.pathname.split('/').slice(-1)[0]
    dispatch(setupSurvey(surveyId))
  }, [])

  if (!survey) return null

  const { name, themes, participant_count, response_rate } = survey
  const rate = Math.round(response_rate * 100)

  return (
    <Container>
      <H1>{name}</H1>
      <P>This survey had <strong>{participant_count}</strong> participants, with a participation rate of <strong>{rate}</strong>%.</P>
      <Themes list={themes} />
      <BackButton url="/">Back to Surveys</BackButton>
    </Container>
  )
}

export default SurveyHOC
