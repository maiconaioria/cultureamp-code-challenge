import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
// components
import {
  Container,
  H1,
  Surveys
} from '../components'
// selectors
import { getSurveys } from '../selectors/surveys'
// actions
import { setupSurveys } from '../actions/surveys'

const SurveysHOC = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setupSurveys())
  }, [])

  const list = useSelector(getSurveys)

  return (
    <Container>
      <H1>Surveys</H1>
      <Surveys list={list} />
    </Container>
  )
}

export default SurveysHOC
