// constants
import { URLS } from '../actions/constants'

export const parseSurveyUrl = (surveyId) =>
  URLS.SURVEY.replace(/{id}/, surveyId)

export const getQuestionResponseRate = (responses) => {
  const filledResponses = responses.filter(response => response.response_content !== '')
  const responsesTotal = filledResponses.reduce((acc, response) => {
    acc += Number(response.response_content)
    return acc
  }, 0)

  // NOTE: was unsure if I should display float or int numbers
  return {
    averageRating: Number((responsesTotal / filledResponses.length).toFixed(2).replace(/[.,]00$/, '')),
    // averageRating: Math.round(responsesTotal / filledResponses.length),
    participationRate: Math.round((filledResponses.length / responses.length) * 100)
  }
}
