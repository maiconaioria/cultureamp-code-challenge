import { parseSurveyUrl, getQuestionResponseRate } from './surveys'

describe('survey utils', () => {
  test('parseSurveyUrl is working properly', () => {
    expect(parseSurveyUrl(1)).toBe('https://px2yf2j445.execute-api.us-west-2.amazonaws.com/production/surveys/1')
  })

  test('getQuestionResponseRate is working properly', () => {
    expect(getQuestionResponseRate([
      { response_content: 5 },
      { response_content: 5 },
      { response_content: 5 }
    ])).toMatchObject({
      averageRating: 5,
      participationRate: 100
    })

    expect(getQuestionResponseRate([
      { response_content: 4 },
      { response_content: 4 },
      { response_content: 4 }
    ])).toMatchObject({
      averageRating: 4,
      participationRate: 100
    })

    expect(getQuestionResponseRate([
      { response_content: '' },
      { response_content: '' },
      { response_content: 3 }
    ])).toMatchObject({
      averageRating: 3,
      participationRate: 33
    })
  })
})
