export const fetchApi = (url) =>
  fetch(url)
    .then(response => response.json())
    .then(response => response)
    .catch((error) => handleFetchErrors(error))

export const fetchAll = (requests) =>
  Promise.all(requests)

export const handleFetchErrors = (error) => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log(error.response.data)
    console.log(error.response.status)
    console.log(error.response.headers)
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log(error.message, error.request)
  } else {
    // Something happened in setting up the request that triggered an Error
    console.error(error.message)
  }
  console.log(error.config)
}
