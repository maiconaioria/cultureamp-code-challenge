export const mockSurvey = {
  byId: {
    1: {
      id: 1
    },
    2: {
      id: 2
    }
  }
}

export const mockState = {
  active: false,
  byId: mockSurvey
}

export const newSurvey = {
  id: 2,
  name: 'Acme Engagement Survey',
  url: '/survey_results/2',
  participant_count: 271,
  response_rate: 1,
  submitted_response_count: 271,
  themes: []
}

export const newSurveys = [
  { url: 'surveys/999' },
  { url: 'surveys/666' }
]
