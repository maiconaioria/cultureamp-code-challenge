import React from 'react'
import { render } from '@testing-library/react'
// components
import { Questions } from './Questions'

describe('Questions component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<Questions list={[]} />)
    expect(container.firstChild).toMatchSnapshot()
  })
})
