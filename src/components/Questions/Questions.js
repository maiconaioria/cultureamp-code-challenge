import React from 'react'
import PropTypes from 'prop-types'
// components
import { H3 } from '../Heading'
// selectors
import { getQuestionResponseRate } from '../../utils/surveys'
// styles
import styles from './style.module.scss'

export const Questions = ({
  list = []
}) => {
  if (!list.length) return null

  return (
    <ul className={styles.list}>
      {list.map((question, index) => {
        const { description, survey_responses } = question
        const { averageRating, participationRate } = getQuestionResponseRate(survey_responses)
        return (
          <li key={`question-${index}`} className={styles.item}>
            <H3>
              <span className={styles.title}>{description}</span>
            </H3>
            <div className={styles.info}>
              <span className={styles.infoHeading}>Average rating</span>
              <span className={styles.infoContent}>{averageRating} (of 5)</span>
            </div>
            <div className={styles.info}>
              <span className={styles.infoHeading}>Participation rate</span>
              <span className={styles.infoContent}>{participationRate}%</span>
            </div>
          </li>
        )
      })}
    </ul>
  )
}

Questions.propTypes = {
  list: PropTypes.array
}
