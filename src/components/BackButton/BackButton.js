import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
// styles
import styles from './style.module.scss'

export const BackButton = ({
  url,
  children
}) => {
  return (
    <Link to={url} className={styles.button}>{children}</Link>
  )
}

BackButton.propTypes = {
  url: PropTypes.string,
  children: PropTypes.node.isRequired
}
