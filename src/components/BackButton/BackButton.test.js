import React from 'react'
import { render } from '@testing-library/react'
import { BrowserRouter as Router } from 'react-router-dom'
// components
import { BackButton } from './BackButton'

describe('BackButton component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<Router><BackButton url="/">Go Back</BackButton></Router>)
    expect(container.firstChild).toMatchSnapshot()
  })
})
