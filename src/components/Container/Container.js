import React from 'react'
import PropTypes from 'prop-types'
// styles
import styles from './style.module.scss'

export const Container = ({
  children
}) => {
  return (
    <div className={styles.container}>{children}</div>
  )
}

Container.propTypes = {
  children: PropTypes.node.isRequired
}
