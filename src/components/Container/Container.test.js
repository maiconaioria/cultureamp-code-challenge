import React from 'react'
import { render } from '@testing-library/react'
// components
import { Container } from './Container'

describe('Container component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<Container>Hello world</Container>)
    expect(container.firstChild).toMatchSnapshot()
  })
})
