import React from 'react'
import PropTypes from 'prop-types'
// components
import { Questions } from '../Questions'
import { H2 } from '../Heading'
// styles
import styles from './style.module.scss'

export const Themes = ({
  list = []
}) => {
  if (!list.length) return null

  return (
    list.map((theme, index) => {
      const { name, questions } = theme
      return (
        <ul key={`theme-${index}`} className={styles.list}>
          <li className={styles.item}>
            <H2>{name}</H2>
            <Questions list={questions} />
          </li>
        </ul>
      )
    })
  )
}

Themes.propTypes = {
  list: PropTypes.array
}
