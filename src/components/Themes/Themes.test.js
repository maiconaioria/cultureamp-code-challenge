import React from 'react'
import { render } from '@testing-library/react'
// components
import { Themes } from './Themes'

describe('Themes component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<Themes list={[]} />)
    expect(container.firstChild).toMatchSnapshot()
  })
})
