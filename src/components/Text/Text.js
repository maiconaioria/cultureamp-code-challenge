import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
// constants
import { TEXT_TAGS } from './constants'
// styles
import styles from './style.module.scss'

const Text = ({
  renderAs = TEXT_TAGS.H2,
  children
}) => {
  if (typeof children === 'undefined') return null

  const HTMLTag = renderAs

  const className = classNames(styles.text, {
    [styles.block]: HTMLTag === TEXT_TAGS.DIV || HTMLTag === TEXT_TAGS.P
  })

  return <HTMLTag className={className}>{children}</HTMLTag>
}

Text.propTypes = {
  renderAs: PropTypes.string,
  children: PropTypes.node.isRequired
}

export const P = ({ children, renderAs, ...otherProps }) =>
  <Text renderAs={TEXT_TAGS.P} {...otherProps} >{children}</Text>

export const Div = ({ children, renderAs, ...otherProps }) =>
  <Text renderAs={TEXT_TAGS.DIV} {...otherProps} >{children}</Text>

export const Span = ({ children, renderAs, ...otherProps }) =>
  <Text renderAs={TEXT_TAGS.SPAN} {...otherProps} >{children}</Text>

P.propTypes = Text.propTypes
Div.propTypes = Text.propTypes
Span.propTypes = Text.propTypes
