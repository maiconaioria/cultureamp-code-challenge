import React from 'react'
import { render } from '@testing-library/react'
// components
import { P, Div, Span } from './Text'

describe('P component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<P>Hello world</P>)
    expect(container.firstChild).toMatchSnapshot()
  })
})

describe('Div component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<Div>Hello world</Div>)
    expect(container.firstChild).toMatchSnapshot()
  })
})

describe('Span component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<Span>Hello world</Span>)
    expect(container.firstChild).toMatchSnapshot()
  })
})
