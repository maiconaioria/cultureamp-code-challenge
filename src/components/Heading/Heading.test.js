import React from 'react'
import { render } from '@testing-library/react'
// components
import { H1, H2, H3, H4, H5, H6 } from './Heading'

describe('H1 component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<H1>Hello world</H1>)
    expect(container.firstChild).toMatchSnapshot()
  })
})

describe('H2 component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<H2>Hello world</H2>)
    expect(container.firstChild).toMatchSnapshot()
  })
})

describe('H3 component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<H3>Hello world</H3>)
    expect(container.firstChild).toMatchSnapshot()
  })
})

describe('H4 component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<H4>Hello world</H4>)
    expect(container.firstChild).toMatchSnapshot()
  })
})

describe('H5 component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<H5>Hello world</H5>)
    expect(container.firstChild).toMatchSnapshot()
  })
})

describe('H6 component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<H6>Hello world</H6>)
    expect(container.firstChild).toMatchSnapshot()
  })
})
