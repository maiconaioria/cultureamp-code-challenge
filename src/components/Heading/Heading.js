import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
// constants
import { HEADING_TAGS } from './constants'
// styles
import styles from './style.module.scss'

const Heading = ({
  renderAs = HEADING_TAGS.H2,
  children
}) => {
  if (typeof children === 'undefined') return null

  const HTMLTag = renderAs

  const className = classNames(styles.heading, {
    [styles.h1]: HTMLTag === HEADING_TAGS.H1,
    [styles.h2]: HTMLTag === HEADING_TAGS.H2,
    [styles.h3]: HTMLTag === HEADING_TAGS.H3,
    [styles.h4]: HTMLTag === HEADING_TAGS.H4,
    [styles.h5]: HTMLTag === HEADING_TAGS.H5,
    [styles.h6]: HTMLTag === HEADING_TAGS.H6
  })

  return <HTMLTag className={className}>{children}</HTMLTag>
}

Heading.propTypes = {
  renderAs: PropTypes.string,
  children: PropTypes.node.isRequired
}

export const H1 = ({ children, renderAs, ...otherProps }) =>
  <Heading renderAs={HEADING_TAGS.H1} {...otherProps} >{children}</Heading>

export const H2 = ({ children, renderAs, ...otherProps }) =>
  <Heading renderAs={HEADING_TAGS.H2} {...otherProps} >{children}</Heading>

export const H3 = ({ children, renderAs, ...otherProps }) =>
  <Heading renderAs={HEADING_TAGS.H3} {...otherProps} >{children}</Heading>

export const H4 = ({ children, renderAs, ...otherProps }) =>
  <Heading renderAs={HEADING_TAGS.H4} {...otherProps} >{children}</Heading>

export const H5 = ({ children, renderAs, ...otherProps }) =>
  <Heading renderAs={HEADING_TAGS.H5} {...otherProps} >{children}</Heading>

export const H6 = ({ children, renderAs, ...otherProps }) =>
  <Heading renderAs={HEADING_TAGS.H6} {...otherProps} >{children}</Heading>

H1.propTypes = Heading.propTypes
H2.propTypes = Heading.propTypes
H3.propTypes = Heading.propTypes
H4.propTypes = Heading.propTypes
H5.propTypes = Heading.propTypes
H6.propTypes = Heading.propTypes
