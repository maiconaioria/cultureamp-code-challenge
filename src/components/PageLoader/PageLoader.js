import React from 'react'
import PropTypes from 'prop-types'
// styles
import styles from './style.module.scss'

export const PageLoader = ({
  isVisible = false
}) => {
  return isVisible && (
    <div className={styles.backdrop}>
      <div className={styles.loader} />
    </div>
  )
}

PageLoader.propTypes = {
  isVisible: PropTypes.bool
}
