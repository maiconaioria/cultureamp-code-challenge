import React from 'react'
import { render } from '@testing-library/react'
// components
import { PageLoader } from './PageLoader'

describe('PageLoader component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<PageLoader isVisible />)
    expect(container.firstChild).toMatchSnapshot()
  })
})
