import React from 'react'
import { render } from '@testing-library/react'
// components
import { Surveys } from './Surveys'

describe('Surveys component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<Surveys list={[]} />)
    expect(container.firstChild).toMatchSnapshot()
  })
})
