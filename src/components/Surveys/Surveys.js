import React from 'react'
import PropTypes from 'prop-types'
// components
import { Survey } from './Survey'
// styles
import styles from './style.module.scss'

export const Surveys = ({
  list = []
}) => {
  if (!list.length) return null

  return (
    <ul className={styles.list}>
      {list.map((survey, index) => <Survey key={`survey-${index}`} {...survey} />)}
    </ul>
  )
}

Surveys.propTypes = {
  list: PropTypes.array
}
