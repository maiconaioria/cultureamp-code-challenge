import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
// components
import { H3 } from '../Heading'
// styles
import styles from './style.module.scss'

export const Survey = ({
  id,
  name
}) => {
  return (
    <li className={styles.item}>
      <H3>
        <Link to={`/survey/${id}`} className={styles.link}>{name}</Link>
      </H3>
    </li>
  )
}

Survey.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string
}
