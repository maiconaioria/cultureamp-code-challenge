import React from 'react'
import { render } from '@testing-library/react'
import { BrowserRouter as Router } from 'react-router-dom'
// components
import { Survey } from './Survey'

describe('Survey component', () => {
  test('matches the snapshot', () => {
    const { container } = render(<Router><Survey id="1" name="Hello world" /></Router>)
    expect(container.firstChild).toMatchSnapshot()
  })
})
