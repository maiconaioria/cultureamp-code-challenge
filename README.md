## Notes

I spent most of the time around the structure and setting up the environment, as I believe it's better to plan well at first before having to do big changes in the future. I have added very simple CSS rules to customize the lists, but they can definitely be improved to be more appealing. I decided to use Redux as I like to have a single source of data easily accessible anywhere on my application. I could have built the application using Typescript without much more effort, but considering how easy it is to get lost trying to fix a type error sometimes, I decided to play safe and leave it for a future iteration. The components tests could also be improved, as most of what I did were only snapshot tests.

## Instructions
Run `npm install` to install the dependencies, and `npm run start` to start the application.\
You can run `npm test` to validate the tests.

## Available Scripts

In the project directory, you can run:

### `yarn start (npm run start)`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test (npm run test)`

Launches the test runner in the interactive watch mode.\